<?php
// Voici mon jeu de la vie, et un petit commentaire des petites améliorations possibles :
// Je n'ai pas encore affiché, sur la page html, pas mal d'infos dont
// le numéro de la génération actuelle qui augmente à chaques générations.
// Je peux rendre plus pratique le refresh avec un bouton sur la page plutot que de F5.
// Plus long mais je pourrais faire en sorte que l'on choisisse les cellules vivantes
// de la première génération. (Pour pouvoir faire ce qu'on veut)
// J'aurais pu faire mieux mais comme c'est la première fois que je code en php,
// j'ai pris beaucoup de temps à connaitre tout ce dont on avait besoin pour ce TP.

session_start();
$taille = [20, 50];

function initialisation_tableau($tableaux, $taille, $random = true) {
    for($x = 0; $x < $taille[0]; $x++) {
        $tableaux[$x] = Array();
        for($y = 0; $y < $taille[1]; $y++) {
            $tableaux[$x][$y] = ($random && (bool) rand(0,1)) ? true : false;
        }
    }
    return $tableaux;
}

function a_un_voisin($tableaux, $taille, $tab) {
    $x = $tab[0];
    $y = $tab[1];
    $voisins = 0;
    if($x != 0 && $y != 0) {if($tableaux[$x-1][$y-1]) {$voisins++;}}
    if($x != 0) {if($tableaux[$x-1][$y]) {$voisins++;}}
    if($x != 0 && $y != $taille[1]-1) {if($tableaux[$x-1][$y+1]) {$voisins++;}}
    if($y != 0) {if($tableaux[$x][$y-1]) {$voisins++;}}
    if($y != $taille[1]-1) {if($tableaux[$x][$y+1]) {$voisins++;}}
    if($x != $taille[0]-1 && $y != 0) {if($tableaux[$x+1][$y-1]) {$voisins++;}}
    if($x != $taille[0]-1) {if($tableaux[$x+1][$y]) {$voisins++;}}
    if($x != $taille[0]-1 && $y != $taille[1]-1) {if($tableaux[$x+1][$y+1]) {$voisins++;}}
    return $voisins;
}

function naissance($tableaux, $tab, $voisins) {
    $x = $tab[0];
    $y = $tab[1];
    return ($voisins === 3 && !$tableaux[$x][$y]);
}

function mort($tableaux, $tab, $voisins) {
    $x = $tab[0];
    $y = $tab[1];
    return (($voisins < 2 OR $voisins > 3) && $tableaux[$x][$y]);
}

function changements($tableaux, $taille) {
    $en_cours = ['naissance' => Array(), 'mort' => Array()];
    for($x = 0; $x < $taille[0]; $x++) {
        for($y = 0; $y < $taille[1]; $y++) {
            $tab = [$x, $y];
            $voisins = a_un_voisin($tableaux, $taille, $tab);
            if(naissance($tableaux, $tab, $voisins)) {array_push($en_cours['naissance'], $tab);}
            if(mort($tableaux, $tab, $voisins)) {array_push($en_cours['mort'], $tab);}
        }
    }
    return $en_cours;
}

function decision_finale($tableaux, $en_cours) {
    foreach ($en_cours['naissance'] as $tab) {
        $x = $tab[0];
        $y = $tab[1];
        $tableaux[$x][$y] = true;
    }
    foreach ($en_cours['mort'] as $tab) {
        $x = $tab[0];
        $y = $tab[1];
        $tableaux[$x][$y] = false;
    }
    return $tableaux;
}

function generation_suivante($tableaux, $taille) {
    $en_cours = changements($tableaux, $taille);
    $tableaux = decision_finale($tableaux, $en_cours); 
    $_SESSION['generation_presente']++;
    return $tableaux;
}

if(!isset($_SESSION['tableaux'])) { 
    $tableaux = initialisation_tableau(Array(), $taille);
    $_SESSION['tableaux'] = $tableaux;
    $_SESSION['taille'] = $taille;
    $_SESSION['generation_presente'] = 0;
} 
else {
    if(isset($generation) && @$generation > $_SESSION['generation_presente']) {
        $tableaux = $_SESSION['tableaux'];
        $taille = $_SESSION['taille'];
        for($g = $_SESSION['generation_presente']; $g < $generation; $g++) {
            $tableaux = generation_suivante($tableaux, $taille);
        }
    } 
    else {
        $tableaux = generation_suivante($_SESSION['tableaux'], $_SESSION['taille']);
    }
    $_SESSION['tableaux'] = $tableaux;
}
?>