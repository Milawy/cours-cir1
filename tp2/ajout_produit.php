<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <title>Ajout de produit</title>
    </head>
    <body>
        <?php
        if(isset($_POST['nomproduit'])){
            $mesProduits= fopen('mesProduits.csv', 'a+');
            fputs($mesProduits, $_POST['nomproduit'].';'.$_POST['prix'].';'.$_POST['stock'].';'.$_FILES['image']['name'].$_FILES['image']['extension']."\n");
            fclose($mesProduits);
            }
        ?>
        <h1>Ajoutez votre produit</h1>
        <form action="./ajout_produit.php" method="POST" enctype="multipart/form-data">
        <fieldset>
            <p>
                <label for="Nom">Nom du produit</label><br/>
                <input type="text" name="nomproduit" id="nomproduit"/>
            </p>
            <p>
                <label for="Prix">Prix</label><br/>
                <input type="number" name="prix" id="nomproduit" min="0" step="0.01"/>
                <label for="Prix">€</label><br/>
            </p>
            <p>
                <label for="Nombre en stock">Nombre en stock</label><br/>
                <input type="number" name="stock" id="nomproduit" min="0"/>
            </p>
            <p>
                <label for="image">Votre image</label><br/>
                <input type="file" name="image" id="image"/>
                <?php
                    define('TARGET_DIRECTORY', './images/');
                    if(!empty($_FILES['image'])) {
                        move_uploaded_file($_FILES['image']['tmp_name'], TARGET_DIRECTORY . $_FILES['image']['name']);
                    }
                ?>
            </p>
        </fieldset>
        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </form>
    </body>
</html>