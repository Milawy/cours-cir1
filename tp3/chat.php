<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Shrodinger's Chat</title>
    </head>
    <body>
        <form action="post.php" method="post">
            <p>
                <label for="login">Pseudo</label> : <input type="text" name="login" id="login"/><br/><br/>
                <label for="message">Message</label> :  <input type="text" name="message" id="message"/><br/><br/>
                <input type="submit" value="Envoyer"/><br/><br/>
            </p>
        </form>
        <?php
            try {
                    $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'wxcvbn13579');
                    $reponse = $bdd->query('SELECT login, message FROM chat');
                    foreach($reponse as $row) {
                        echo '<strong>' . htmlspecialchars($row['login']) . '</strong>' . ':' . htmlspecialchars($row['message']);
                    }
            }
            catch(Exception $e) {
                    exit('Erreur de connexion à la base de données');
            }
        ?>
    </body>
</html>