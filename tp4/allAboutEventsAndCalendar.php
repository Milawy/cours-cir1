<?php

if(!isset($_SESSION['date'])) {
    $_SESSION['date'] = strtotime('today');
}

function today($date, $i) {
    $day = monthDay($date, $i);
    $today = date('Y/m/', $date);
    $today .= ($day < 10) ? '0' : '';
    $today .= $day;
    return $today;
}

function monthName($timestamp) {
    $months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
    $idx = ltrim(date('m', $timestamp), '0') - 1;
    return $months[$idx];
}

function year($timestamp) {
    return date('Y', $timestamp);
}

function startDay($timestamp) {
    $year = explode("/", date('Y', $timestamp));
    $month = explode("/", date('m', $timestamp));
    return date('d', mktime(0, 0, 0, $month, 1, $year));
}

function monthDay($date, $i) {
    $days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
    $start = startDay($date);
    $thisDay = $i - array_search($start, $days) + 1;
    if($thisDay < 1 || $thisDay > date('t', $date)) {
        return null;
    }
    return $thisDay;
}

function redirect($page = './', $message = '', $type = 'warning') {
    if($message != '') {
        $_SESSION['error'] = $message;
        $_SESSION['alert_type'] = $type;
    }
    header('location : ' . $page);
    die();
}

function dayCustomer($db, $date, $i, $userID) {
    $nbEvents = 0;
    $day = monthDay($date, $i);
    $today = today($date, $i);
    $events = events($db, $today);
    foreach ($events as $event) {
        if(!isEventFull($db, $event)) {
            if($nbEvents < 5) {
                $type = (userThatParticipate($db, $event['id'], $user_id)) ? 'warning' : 'info';
                $html = htmlspecialchars($event['id'], ENT_QUOTES) . $type . htmlspecialchars($event['name'], ENT_QUOTES);
            }
            $nbEvents++;
        }
    }
    if($n > 5) {
        $html = '<a href="?show=' . $today . '">Voir plus</a>';
    }
    if($nbEvents == 0) {
        $html = "Il n'y a pas d'événement";
    }
    if($day == null) {
        return null;
    }
    return $day . $html ;
}

function dayOrganizer($db, $date, $i, $organizerID) {
    $day = monthDay($date, $i);
    $today = today($date, $i);
    $events = events($db, $today, $organizerID, 5);
    foreach ($events as $event) {
        $html = htmlspecialchars($event['id'], ENT_QUOTES) . htmlspecialchars($event['name'], ENT_QUOTES);
    }
    if (nbEvents($db, $today, $organizerID) > 5){
        $html = '<a href="?show=' . $today . '">Voir plus</a>';
    }
    if(nbEvents($db, $today, $organizerID) == 0) {
        $html = "Il n'y a pas d'événement";
    }
    $html = '<a href="?add=' . $today . '">Ajouter un événement</a>';
    if($day == null) {
        return null;
    }
    return $day . $html ;
}

function eventCustomer($db, $event, $userID) {
    $full = (isEventFull($db, $event)) ? 'Evénement complet' : '';
    $participate = (userThatParticipate($db, $event['id'], $userID)) ?
        '<p>Vous participez à cet événement</p>': '';

    return '<div><strong>' . htmlspecialchars($event['name'], ENT_QUOTES) . '<strong></div>
            <div>
            <p>Utilisateurs participants :' . eventUserParticipate($db, $event['id']) . '/' . htmlspecialchars($event['nb_place'], ENT_QUOTES) . ' ' . $full . "</p>
            <p>Description de l'événement :" . htmlspecialchars($event['description'], ENT_QUOTES) . "</p>
            <p>Début de l'événement :" . htmlspecialchars($event['startdate'], ENT_QUOTES) . "
               Fin de l'événement :" . htmlspecialchars($event['enddate'], ENT_QUOTES) . '</p>
            </div>';
}

function eventOrganizer($db, $event) {
    $full = (isEventFull($db, $event)) ? 'Evénement complet' : '';
    return '<div><strong>' . htmlspecialchars($event['name'], ENT_QUOTES) . '<strong></div>
            <div>
            <p>Utilisateurs participants :' . eventUserParticipate($db, $event['id']) . '/' . htmlspecialchars($event['nb_place'], ENT_QUOTES) . ' ' . $full . "</p>
            <p>Description de l'événement :" . htmlspecialchars($event['description'], ENT_QUOTES) . "</p>
            <p>Début de l'événement :" . htmlspecialchars($event['startdate'], ENT_QUOTES) . "
               Fin de l'événement :" . htmlspecialchars($event['enddate'], ENT_QUOTES) . '</p>
            </div>';
}