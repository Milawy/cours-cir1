<?php

session_start();

// We include all functions that make the date, the events, ...
include('allAboutEventsAndCalendar.php');

// We include all the models
include('models/bdd.php');
include('models/events.php');
include('models/users.php');

// If you're connected and want to logout
if(isConnected() && isset($_GET['logout'])) {
    session_destroy(); redirect('./');
}

$db = connectBDD();

// The view part of the calendar
include('views/seeTheCalendar.php');

// Are organizer or customer connected ? if not, login
if(isConnected()) {
    $user= user($db, $_SESSION['id']);
    if($user['rank'] == 'ORGANIZER') {
        include('controllers/organizer.php');
    }
    if($user['rank'] == 'CUSTOMER') {
        include('controllers/customer.php');
    }
} 
else {
    include('controllers/login.php');
}