<?php

$date = $_SESSION['date'];

if(isset($_GET['previous'])) {
    $_SESSION['date'] = strtotime('-1 month', $date);
    redirect('./');
}

if(isset($_GET['next'])) {
    $_SESSION['date'] = strtotime('+1 month', $date);
    redirect('./');
}

// Conditions for events
if(isset($_GET['show'])) {
    $day = filter_input(INPUT_GET, 'show');
    $events = events($db, $day);
    if($events == false) {
        redirect('./', "Il n'y a pas d'événement ce jour", 'warning');
    }
    include('views/eventsCustomer.php');
} 
else if(isset($_GET['event'])) {
    $eventID = filter_input(INPUT_GET, 'event');
    $event = event($db, $eventID);
    if($event == false) {
        redirect('./', "L'événement est introuvable", 'warning');
    }
    include('views/showEventToCustomer.php');
} 
else {
    include('views/calendarForCustomer.php');
}