<?php

$date = $_SESSION['date'];

if(isset($_GET['previous'])) {
    $_SESSION['date'] = strtotime('-1 month', $date);
    redirect('./');
}

if(isset($_GET['next'])) {
    $_SESSION['date'] = strtotime('+1 month', $date);
    redirect('./');
}

// Conditions for events
if(isset($_GET['show'])) {
    $day = filter_input(INPUT_GET, 'show');
    $events = events($db, $day, $user['id']);
    if($events == false) {
        redirect('./', "L'événement est introuvable", 'warning');
    }

    include('views/eventsOrganizer.php');
} else if(isset($_GET['event'])) {
    $eventID = filter_input(INPUT_GET, 'event');
    $event = event($db, $eventID);
    if($event == false) {
        redirect('./', "L'événement est introuvable", 'warning');
    }
    include('views/showEventToOrganizer.php');
}
else if(isset($_GET['add'])) {
    if(isset($_GET['submitted'])) {
        $name = filter_input(INPUT_POST, 'name');
        $about = filter_input(INPUT_POST, 'description');
        $start = str_replace('T', ' ', filter_input(INPUT_POST, 'startdate')) . ':00';
        $end = str_replace('T', ' ', filter_input(INPUT_POST, 'enddate')) . ':00';
        $organizerID = $user['id'];
        $nbPlaces = filter_input(INPUT_POST, 'nb_place');
        if(addEvent($db, $name, $about, $start, $end, $organizerID, $nbPlaces)) {
            redirect('./', "L'événement a bien été créé", 'success');
        } 
        else {
            redirect('./', 'Impossible de créer cet événement', 'warning');
        }
    }
    else {
        $day = filter_input(INPUT_GET, 'add');
        include('views/addEvent.php');
    }
} 
else {
    include('views/calendarForOrganizer.php');
}
