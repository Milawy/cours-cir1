<?php

if(isset($_GET['login'])) {
    $username = filter_input(INPUT_POST, 'username');
    $password = filter_input(INPUT_POST, 'password');

    if(verifyUser($db, $username, $password)) {
        redirect('./');
    } else {
        redirect('./','Mauvais Username ou Mot de passe',  'warning');
    }
} 
else {
    include('views/login.php');
}