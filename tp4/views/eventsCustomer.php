<div>
    <h1>Événements du <?= htmlspecialchars(date_format(date_create($day), 'd/m/Y'), ENT_QUOTES); ?></h1>
    <?php foreach ($events as $event): ?>
        <?= eventCustomer($db, $event, $user['id']); ?>
    <?php endforeach; ?>
</div>

