<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
            <title>Calendar</title>
        </head>
        <body>
            <!--Connection part-->
            <div>
                <h1>Connexion</h1>
                <form method="post" action="./?login">
                    <label for="username">Nom d'utilisateur : </label>
                    <input type="text" id="username" name="username" placeholder="Nom d'utilisateur" required>
                    <br/><br/>
                    <label for="password">Mot de passe</label>
                    <input type="password" id="password" name="password" placeholder="Mot de passe" required>
                    <br/><br/>
                    <input type="submit" value="Se connecter">
                </form>
            </div>
        </body>
    </html>