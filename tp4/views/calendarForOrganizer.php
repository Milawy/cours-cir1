<div>
    <div>
        <div>
            <a href="./?previous">Précédent</a></div>
        <div>
            <h1><?= htmlspecialchars(monthName($date), ENT_QUOTES); ?> <?= htmlspecialchars(year($date), ENT_QUOTES); ?></h1>
        </div>
        <div><a href="./?next">Suivant</a></div>
    </div>

    <table>
        <thead>
            <tr>
                <th>Lundi</th>
                <th>Mardi</th>
                <th>Mercredi</th>
                <th>Jeudi</th>
                <th>Vendredi</th>
                <th>Samedi</th>
                <th>Dimanche</th>
            </tr>
        </thead>
        <tbody>
            <?php for($i = 0; $i < 6; $i++): ?>
            <tr>
            <?php for($j = 0; $j < 7; $j++): ?>
                <td>
                    <?= dayOrganizer($db, $date, $i*7 + $j, $user['id']); ?>
                </td>
            <?php endfor; ?>
            </tr>
            <?php endfor; ?>
        </tbody>
    </table>

</div>