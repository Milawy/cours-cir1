<div>
    <h1>Événement (<?= htmlspecialchars($event['id'], ENT_QUOTES) ?>) du <?= htmlspecialchars(date_format(date_create($event['startdate']), 'd/m/Y'), ENT_QUOTES); ?></h1>
    <?= eventOrganizer($db, $event); ?>
</div>

