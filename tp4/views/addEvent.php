<div>
    <h1>Ajouter un événement</h1>
    <form method="post" action="./?add&submitted">
        <input type="text" name="name" placeholder="Nom de l'événement" required>
        <br/>
        <textarea name="description" placeholder="Description de l'événement" required></textarea>
        <br/>
        <div>
            <div>
                <label for="nbPlaces">Nombre de places</label>
            </div>
            <div>
                <input type="number" name="nbPlaces" value="10" required>
            </div>
        </div>
        <br/>
        <div>
            <div>
                <label for="startdate">Début de l'événement</label>
            </div>
            <div>
                <input type="datetime-local" name="start" value="<?= htmlspecialchars($day, ENT_QUOTES); ?>T06:00" required>
            </div>
        </div>
        <br/>
        <div>
            <div>
                <label for="end">Fin de l'événement</label>
            </div>
            <div>
                <input type="datetime-local" name="end" value="<?= htmlspecialchars($day, ENT_QUOTES); ?>T23:00" required>
            </div>
        </div>
        <br/>
        <input type="submit" value="Créer l'événement">
    </form>
</div>
