<div>
    <h1>Événements du <?= htmlspecialchars(date_format(date_create($day), 'd/m/Y'), ENT_QUOTES); ?></h1>
    <?php foreach ($events as $event): ?>
        <?= eventOrganizer($db, $event); ?>
    <?php endforeach; ?>
</div>

