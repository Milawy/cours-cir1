<?php

//Connection to data base
function connectBDD() {
    try {
        $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
        $db = new PDO('mysql:host=localhost;dbname=event_calendar;charset=utf8', 'root', '', $opts);
    } 
    catch (PDOException $e) {
        die('Erreur lors de la connexion à la base de donnée : ' . $e->getMessage());
    }
    return $db;
}