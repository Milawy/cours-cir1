<?php

function isConnected() {
    if(isset($_SESSION['id'])) {
        return true;
    } 
    else {
        return false;
    }
}

function verifyUser(PDO $db, $username, $password) {
    $request = $db->prepare('SELECT * FROM Users WHERE login = :login');
    $request->execute(array(':login' =>  $username));
    if($request->rowCount() < 1) {
        return false;
    }
    $userData = $request->fetch();
    if(password_verify($password, $userData['password'])) {
        $_SESSION['id'] = $userData['id'];
        return true;
    }
    return false;
}

function user(PDO $db, $id) {
    $req = $db->prepare('SELECT * FROM Users WHERE id = :id');
    $req->execute(array(':id' => $id));
    return $request->fetch();
}