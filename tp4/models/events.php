<?php

function events(PDO $db, $day, $organizerID = null, $limit = null) {
    $mysql = 'SELECT * FROM events WHERE DATE(startdate) = :date';
    if($organizerID != null) {
        $mysql = ' AND organizer_id = :organizer_id';
    }
    if($limit != null) {
        $mysql = ' LIMIT ' . $limit;
    }
    $data = [':date' => $day];
    if($organizerID != null) {$data[':organizer_id'] = $organizerID;}
    $request = $db->prepare($mysql);
    $request->execute($data);
    return $request->fetchAll();
}

function event(PDO $db, $eventID) {
    $request = $db->prepare('SELECT * FROM events WHERE id = :id');
    $request->execute(array(':id' => $eventID));
    return $request->fetch();
}

function addEvents(PDO $db, $name, $about, $start, $end, $organizerID, $nbPlaces) {
    $request = $db->prepare('INSERT INTO events (name, description, startdate, enddate, organizer_id, nb_place) VALUE (:name, :description, :startdate, :enddate, :organizer_id, :nb_place)');
    $req = $request->execute(array(':name' => $name, ':about' => $about, ':startdate' => $start, ':enddate' => $end, ':organizer_id' => $organizerID, ':nb_place' => $nbPlaces));
    return $req;
}

function eventUserParticipate(PDO $db, $eventID) {
    $request = $db->prepare('SELECT COUNT(*) FROM user_participates_events WHERE id_event = :id_event');
    $request->execute(array(':id_event' => $eventID));
    return $request->fetch()[0];
}

function isEventFull(PDO $db, $event) {
    $eventID = $event['id'];
    $nbPlaces = $event['nb_place'];
    return (eventUserParticipate($db, $eventID) == $nbPlaces);
}

function userThatParticipate(PDO $db, $eventID, $userID) {
    $request = $db->prepare('SELECT * FROM events e INNER JOIN user_participates_events u WHERE e.id = u.id_event AND e.id = :id_event AND u.id_participant = :id_user');
    $request->execute(array(':id_event' => $eventID, ':id_user' => $userID));
    return $request->rowCount();
}

function nbEvents(PDO $db, $day, $organizerID) {
    $request = $db->prepare('SELECT COUNT(*) FROM events WHERE DATE(startdate) = :date AND organizer_id = :organizer_id');
    $request->execute(array(':date' => $day, ':organizer_id' => $organizerID));
    return $request->fetch()[0];
}
